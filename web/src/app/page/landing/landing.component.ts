import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    //Get the button
    let mybutton = document.getElementById("btn-back-to-top");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () {
      scrollFunction();
    };

    function scrollFunction() {
      if (
        document.body.scrollTop > 20 ||
        document.documentElement.scrollTop > 20
      ) {
        if (mybutton != null)
          mybutton.style.display = "block";
      } else {
        if (mybutton != null)
          mybutton.style.display = "none";
      }
    }
    // When the user clicks on the button, scroll to the top of the document
    if (mybutton != null)
      mybutton.addEventListener("click", backToTop);

    function backToTop() {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    }

  }

  public irA(tag : string, offset : number = 0){
    const element = document.getElementById(tag);
    
    if(element){
      window.scrollTo(window.scrollX, element.offsetTop + offset);
    }
  }





}
