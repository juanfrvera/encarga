import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './page/admin.component';
import { ItemComponent } from './page/item/item.component';
import { CategoriaComponent } from './page/categoria/categoria.component';
import { DashboardComponent } from './page/dashboard/dashboard.component';
import { AuthGuard } from './guard/auth.guard';
import { LoginComponent } from './page/login/login.component';
import { ComercioSelectorComponent } from './page/comercio-selector/comercio-selector.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      { path: '', component: DashboardComponent },
      { path: 'categoria', component: CategoriaComponent },
      { path: 'item', component: ItemComponent }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'elegirComercio',
    component: ComercioSelectorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComercianteRoutingModule { }
